package com.kafka.consumerspring.service;

import com.kafka.consumerspring.dto.response.PaymentStatusMessageResponseDTO;

import java.util.List;

public interface PaymentStatusMessagingService {

    List<PaymentStatusMessageResponseDTO> getMessages();

    void clearCache();

}
