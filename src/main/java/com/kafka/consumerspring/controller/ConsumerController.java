package com.kafka.consumerspring.controller;

import com.kafka.consumerspring.service.PaymentStatusMessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/consumer")
public class ConsumerController {

    @Autowired
    PaymentStatusMessagingService paymentStatusMessagingService;

    @GetMapping(value = "/m/all")
    public ResponseEntity<?> getAllMessages() {
        return ResponseEntity.ok(paymentStatusMessagingService.getMessages());
    }

    @DeleteMapping(value = "/m/all")
    public ResponseEntity<?> clearCache() {
        paymentStatusMessagingService.clearCache();
        return ResponseEntity.ok("\"Messages were cleared\"");
    }

}
